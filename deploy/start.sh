#!/bin/sh

set -e

keytool -import \
  -alias cluster-ca \
  -file /cluster-certs/ca.crt \
  -keystore "$JAVA_HOME/lib/security/cacerts" \
  -noprompt

java -jar \
  "$1"