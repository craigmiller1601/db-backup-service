data "keycloak_realm" "apps_dev" {
  realm = "apps-dev"
}

data "keycloak_realm" "apps_prod" {
  realm = "apps-prod"
}

locals {
  client_common = {
    client_id = "db-backup-service"
    name = "db-backup-service"
    enabled = true
    access_type = "CONFIDENTIAL"
    service_accounts_enabled = true
  }

  access_role_common = {
    name = "access"
  }
}

import {
  to = keycloak_openid_client.db_backup_service_dev
  id = "apps-dev/a2d1def0-5c76-4e30-beb4-75958ad706f0"
}

resource "keycloak_openid_client" "db_backup_service_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = local.client_common.client_id
  name = local.client_common.name
  enabled = local.client_common.enabled
  access_type = local.client_common.access_type
  service_accounts_enabled = local.client_common.service_accounts_enabled
}

import {
  to = keycloak_openid_client.db_backup_service_prod
  id = "apps-prod/1d9760b9-1cdb-4a49-9ff9-fac415b1a52c"
}

resource "keycloak_openid_client" "db_backup_service_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = local.client_common.client_id
  name = local.client_common.name
  enabled = local.client_common.enabled
  access_type = local.client_common.access_type
  service_accounts_enabled = local.client_common.service_accounts_enabled
}

import {
  to = keycloak_role.db_backup_service_access_role_dev
  id = "apps-dev/51b678ed-9455-450a-b524-baedc2c95324"
}

resource "keycloak_role" "db_backup_service_access_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = keycloak_openid_client.db_backup_service_dev.id
  name = local.access_role_common.name
}

import {
  to = keycloak_role.db_backup_service_access_role_prod
  id = "apps-prod/6bc4427f-6c4f-4ea4-9f4f-7fac0b4a3f06"
}

resource "keycloak_role" "db_backup_service_access_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = keycloak_openid_client.db_backup_service_prod.id
  name = local.access_role_common.name
}