data "keycloak_openid_client" "email_service_client_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = "email-service"
}

data "keycloak_openid_client" "email_service_client_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = "email-service"
}

data "keycloak_role" "email_service_access_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = data.keycloak_openid_client.email_service_client_dev.id
  name = local.access_role_common.name
}

data "keycloak_role" "email_service_access_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = data.keycloak_openid_client.email_service_client_prod.id
  name = local.access_role_common.name
}

import {
  to = keycloak_openid_client_service_account_role.db_backup_service_email_service_access_dev
  id = "apps-dev/f7bde27d-f308-4189-bedb-6818391f7bc7/98ddff56-205d-45a9-931d-1afea35b109e/6da86d0d-7442-442a-93c2-e20601f7262f"
}

resource "keycloak_openid_client_service_account_role" "db_backup_service_email_service_access_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  service_account_user_id = keycloak_openid_client.db_backup_service_dev.service_account_user_id
  client_id = data.keycloak_openid_client.email_service_client_dev.id
  role = local.access_role_common.name
}

import {
  to = keycloak_openid_client_service_account_role.db_backup_service_email_service_access_prod
  id = "apps-prod/815dbf2b-fe3b-4f82-9a73-0a8574b411bc/e1b642e4-8fe6-4ba6-a7fc-05eb19fd9c86/81db67ff-02e9-4529-8710-c35c841d7b6e"
}

resource "keycloak_openid_client_service_account_role" "db_backup_service_email_service_access_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  service_account_user_id = keycloak_openid_client.db_backup_service_prod.service_account_user_id
  client_id = data.keycloak_openid_client.email_service_client_prod.id
  role = local.access_role_common.name
}