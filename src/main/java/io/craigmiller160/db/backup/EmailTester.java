package io.craigmiller160.db.backup;

import io.craigmiller160.db.backup.email.EmailService;
import io.craigmiller160.db.backup.properties.PropertyReader;

public class EmailTester {
  public static void main(final String[] args) {
    new PropertyReader()
        .readProperties()
        .map(EmailService::new)
        .onSuccess(
            emailService -> emailService.sendMongoErrorAlertEmail("testing", new Exception()));
  }
}
